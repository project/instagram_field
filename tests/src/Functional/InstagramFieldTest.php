<?php

namespace Drupal\Tests\instagram_field\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * Tests that the field works.
 *
 * @group instagram_field
 */
class InstagramFieldTest extends BrowserTestBase {
  use SchemaCheckTestTrait;
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'instagram_field',
    'field_ui',
  ];

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Use the standard profile.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $admin;

  /**
   * Node one.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node1;

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Client ID.
   *
   * @var string
   */
  const CLIENTID = '0123456789';

  /**
   * Client secret.
   *
   * @var string
   */
  const CLIENTSECRET = '987654321';

  /**
   * Instagram API initial code.
   *
   * @var string
   */
  const INITIALCODE = 'c0de';

  /**
   * Instagram API access token.
   *
   * @var string
   */
  const ACCESSTOKEN = '9876543210ABCDEF';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($this->admin);

    // Module configuration.
    $this->drupalGet('admin/config/services/instagram_field');
    $this->submitForm([
      'clientid' => self::CLIENTID,
      'clientsecret' => self::CLIENTSECRET,
    ], 'Save configuration');

    $this->drupalGet($this->baseUrl . '/_instagram_field_callback?code=' . self::INITIALCODE);
    $this->assertSession()->elementTextContains('css', '#edit-accesstoken .description', self::ACCESSTOKEN);

    // Create content type.
    $this->createContentType([
      'name' => 'Page',
      'type' => 'page',
    ]);
    $this->drupalGet('admin/structure/types/manage/page/fields/add-field');

    // Field configuration.
    $this->submitForm([
      'new_storage_type' => 'instagramfield',
    ], 'Continue');
    $this->submitForm([
      'label' => 'instagramrecent',
      'field_name' => 'instagramrecent',
    ], 'Continue');

    $this->submitForm([
      'field_storage[subform][cardinality]' => 'number',
      'field_storage[subform][cardinality_number]' => '1',
    ], 'Save settings');

    $this->drupalGet('admin/structure/types/manage/page/form-display');
    $edit = [
      'fields[field_instagramrecent][type]' => 'instagramfield_default',
      'fields[field_instagramrecent][region]' => 'content',
      'fields[field_instagramrecent][weight]' => '110',
      'fields[field_instagramrecent][parent]' => '',
    ];
    $this->submitForm($edit, 'Save');

    $this->drupalGet('admin/structure/types/manage/page/display');
    $edit1 = [
      'fields[field_instagramrecent][region]' => 'content',
      'fields[field_instagramrecent][label]' => 'above',
      'fields[field_instagramrecent][type]' => 'instagramfield_formatter',
      'fields[field_instagramrecent][weight]' => '110',
      'fields[field_instagramrecent][parent]' => '',
    ];
    $this->submitForm($edit1, 'Save');

    // Create node.
    $this->node1 = $this->drupalCreateNode([
      'title' => 'Node one',
      'type' => 'page',
    ]);
  }

  /**
   * Test instagram field.
   */
  public function testField() {
    $this->drupalLogin($this->admin);

    $this->drupalGet('/node/' . $this->node1->id() . '/edit');
    $this->submitForm([
      'body[0][value]' => 'testbody',
    ], 'Save');

    // Does field exists in frontend.
    $this->drupalGet('/node/' . $this->node1->id());
    $this->assertSession()->elementExists('css', 'A[href^="https://www.instagram.com"]');
    $this->assertSession()->elementAttributeContains('css', 'A[href^="https://www.instagram.com"]', 'class', 'instagram-field-VIDEO');
  }

}
