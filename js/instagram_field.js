/**
 * @file
 */

(($, Drupal) => {
  Drupal.behaviors.instagramfieldsettings = {
    attach() {
      $('#instagram-fieldsettings #edit-auth').on('click', (e) => {
        const formdata = $('#instagram-fieldsettings').serializeArray();
        formdata.push({
          name: 'op',
          value: $('#instagram-fieldsettings #edit-submit').val(),
        });
        $.post($('#instagram-fieldsettings').attr('action'), formdata, () => {
          const clientid = $('#instagram-fieldsettings #edit-clientid').val();
          const description = encodeURIComponent(
            $('#instagram-fieldsettings #edit-callbackurl--description')
              .text()
              .replace(/\s+/g, ''),
          );
          const url = `https://api.instagram.com/oauth/authorize/?client_id=${clientid}&redirect_uri=${description}&response_type=code&scope=user_profile,user_media`;
          window.open(url);
        });
        e.preventDefault();
      });
    },
  };
})(jQuery, Drupal);
