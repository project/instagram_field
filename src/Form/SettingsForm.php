<?php

namespace Drupal\instagram_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a form to configure module settings for instagram field.
 */
class SettingsForm extends ConfigFormBase {
  /**
   * The variable containing the request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Dependency injection through the constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(RequestStack $requestStack, MessengerInterface $messenger) {
    $this->requestStack = $requestStack;
    $this->messenger = $messenger;
  }

  /**
   * Dependency injection create.
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('request_stack'),
    $container->get('messenger'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instagram_field.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config.instagram_field'];
  }

  /**
   * Settings form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;
    $config = $this->config('config.instagram_field');
    if ($config->get('accesstokenexpire') < time()) {
      $this->messenger->addMessage($this->t('Authenticate your Instagram app.'), 'warning');
    }
    $form['#attached']['library'][] =
      'instagram_field/instagram_field-scripts';

    $form['callbackurl'] = [
      '#type' => 'item',
      '#title' => $this->t('OAuth redirect URI @url', ['@url' => '(https://developers.facebook.com)']),
      '#description' => $base_url . '/_instagram_field_callback',
    ];
    $form['clientid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram app ID'),
      '#default_value' => $config->get('clientid'),
    ];
    $form['clientsecret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram app secret'),
      '#default_value' => $config->get('clientsecret'),
    ];
    $form['auth'] = [
      '#type' => 'button',
      '#value' => $this->t('Authenticate'),
      '#ajax' => [],
    ];

    $form['debug'] = [
      '#type' => 'details',
      '#title' => $this->t('Debug'),
      '#open' => FALSE,
    ];
    $form['debug']['accesstoken'] = [
      '#type' => 'item',
      '#title' => $this->t('Instagram access token'),
      '#description' => $config->get('accesstoken'),
    ];
    $form['debug']['accesstokenrefresh'] = [
      '#type' => 'item',
      '#title' => $this->t('Next access token refresh not before x seconds'),
      '#description' => $config->get('accesstokenrefresh') - time(),
    ];
    $form['debug']['accesstokenexpire'] = [
      '#type' => 'item',
      '#title' => $this->t('Access token expires in x seconds'),
      '#description' => $config->get('accesstokenexpire') - time(),
    ];
    $form['debug']['userid'] = [
      '#type' => 'item',
      '#title' => $this->t('Instagram app user ID'),
      '#description' => $config->get('userid'),
    ];
    $form['cachetime'] = [
      '#type' => 'number',
      '#title' => $this->t('Cache time in minutes'),
      '#default_value' => $config->get('cachetime'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Settings form validation.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Prevent from most common error (spaces before or behind)
    $form_state->setValue('clientid', trim($form_state->getValue('clientid')));
    $form_state->setValue('clientsecret', trim($form_state->getValue('clientsecret')));

    if (preg_match('/[^0-9]/', $form_state->getValue('clientid'))) {
      $form_state->setErrorByName('clientid',
        $this->t('Instagram app ID contains not valid chars [0-9].'));
    }
    if (preg_match('/[^a-f0-9]/', $form_state->getValue('clientsecret'))) {
      $form_state->setErrorByName('clientsecret',
        $this->t('Instagram app secret contains not valid chars [a-f0-9].'));
    }
  }

  /**
   * Settings form submit.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('config.instagram_field');

    $config->set('clientid', $form_state->getValue('clientid'));
    $config->set('clientsecret', $form_state->getValue('clientsecret'));

    $config->set('cachetime', $form_state->getValue('cachetime'));

    $config->save();

    return parent::submitForm($form, $form_state);
  }

}
