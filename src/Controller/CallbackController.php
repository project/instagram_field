<?php

namespace Drupal\instagram_field\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Instagram Field Callback Controller.
 */
class CallbackController extends ControllerBase {

  /**
   * The variable containing the request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The variable containing the logging.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  private $logger;

  /**
   * The variable containing the http client.
   *
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * Dependency injection through the constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger service.
   * @param \GuzzleHttp\Client $httpClient
   *   The http client service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    ConfigFactoryInterface $config,
    RequestStack $requestStack,
    LoggerChannelFactory $logger,
    Client $httpClient,
    MessengerInterface $messenger,
  ) {
    $this->configFactory = $config;
    $this->requestStack = $requestStack;
    $this->logger = $logger;
    $this->httpClient = $httpClient;
    $this->messenger = $messenger;
  }

  /**
   * Dependency injection create.
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'),
    $container->get('request_stack'),
    $container->get('logger.factory'),
    $container->get('http_client'),
    $container->get('messenger'));
  }

  /**
   * Callback from instagram auth with accesstoken.
   */
  public function callback() {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->configFactory->getEditable('config.instagram_field');
    /** @var \Symfony\Component\HttpFoundation\Request $http_request */
    $http_request = $this->requestStack->getCurrentRequest();
    if ($http_request->query->get('code') === NULL) {
      $err_msg = $this->t("instagramautherror: Error no code");
      $this->logger->get('instagram_field')->error($err_msg);
      $this->messenger->addMessage($err_msg, 'error');
      return [
        '#type' => 'markup',
        '#markup' => '',
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    }
    elseif (substr($http_request->query->get('code'), 0, 5) == 'error') {
      $err_msg = $this->t("instagramautherror: @code", [
        '@code' => $http_request->query->get('code'),
      ]);
      $this->logger->get('instagram_field')->error($err_msg);
      $this->messenger->addMessage($err_msg, 'error');
      return [
        '#type' => 'markup',
        '#markup' => '',
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    }
    else {
      $request = $this->httpPost('https://api.instagram.com/oauth/access_token', [
        'form_params' => [
          'client_id' => trim($config->get('clientid')),
          'client_secret' => trim($config->get('clientsecret')),
          'grant_type' => 'authorization_code',
          'redirect_uri' => $http_request->getSchemeAndHttpHost() .
          '/_instagram_field_callback',
          'code' => $http_request->query->get('code'),
        ],
      ]);
      $result = json_decode($request->getBody());
      $config->set('userid', preg_replace('/[^A-Fa-f0-9]/', '', $result->user_id));
      $request = $this->httpGet('https://graph.instagram.com/access_token', [
        'query' => [
          'client_secret' => trim($config->get('clientsecret')),
          'grant_type' => 'ig_exchange_token',
          'access_token' => $result->access_token,
        ],
      ]);
      $result = json_decode($request->getBody());
      $config->set('accesstoken', preg_replace('/[^A-Za-z0-9.]/', '', $result->access_token));
      $config->set('accesstokenexpire', time() + $result->expires_in);
      $config->set('accesstokenrefresh', time() + (24 * 60 * 60));
      $config->save();
    }

    $response = new RedirectResponse(Url::fromRoute('instagram_field.settings')->toString());
    $response->send();
    return $response;
  }

  /**
   * HttpClient get method enable test mock.
   */
  private function httpGet($uri, $options) {
    if (drupal_valid_test_ua()) {
      return new Response(200, [], file_get_contents(dirname(__FILE__) . '/../../tests/src/Functional/Mocks/graph_instagram_comaccess_token.json'));
    }
    else {
      return $this->httpClient->get($uri, $options);
    }
  }

  /**
   * HttpClient post method enable test mock.
   */
  private function httpPost($uri, $options) {
    if (drupal_valid_test_ua()) {
      return new Response(200, [], file_get_contents(dirname(__FILE__) . '/../../tests/src/Functional/Mocks/instagram_oauth_access_token.json'));
    }
    else {
      return $this->httpClient->post($uri, $options);
    }
  }

}
