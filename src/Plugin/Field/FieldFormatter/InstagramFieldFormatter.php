<?php

namespace Drupal\instagram_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'instagramfield_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "instagramfield_formatter",
 *   label = @Translation("Instagram recent"),
 *   field_types = {
 *     "instagramfield"
 *   }
 * )
 */
class InstagramFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The variable containing the conditions configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The variable containing the http client.
   *
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * The variable containing the logging.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  private $logger;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * File url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  private $fileUrlGenerator;

  const THUMBS_DIRECTORY = 'public://instagram_thumbnails';

  /**
   * Dependency injection through the constructor.
   *
   * @param string $plugin_id
   *   The plugin_id.
   * @param mixed $plugin_definition
   *   The plugin_definition.
   * @param mixed $field_definition
   *   The field_definition.
   * @param array $settings
   *   The settings.
   * @param string $label
   *   The label.
   * @param string $view_mode
   *   The view_mode.
   * @param array $third_party_settings
   *   The third_party_settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config service.
   * @param \GuzzleHttp\Client $httpClient
   *   The http client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   The logger service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file url generator service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    ConfigFactoryInterface $config,
    Client $httpClient,
    LoggerChannelFactory $logger,
    FileSystemInterface $fileSystem,
    FileUrlGeneratorInterface $fileUrlGenerator,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->configFactory = $config;
    $this->httpClient = $httpClient;
    $this->logger = $logger;
    $this->fileSystem = $fileSystem;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * Dependency injection create.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id,
    $plugin_definition,
    $configuration['field_definition'],
    $configuration['settings'],
    $configuration['label'],
    $configuration['view_mode'],
    $configuration['third_party_settings'],
    $container->get('config.factory'),
    $container->get('http_client'),
    $container->get('logger.factory'),
    $container->get('file_system'),
    $container->get('file_url_generator'));
  }

  /**
   * Get recent posts from instagram.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    /** @var \Drupal\Core\Config\Config */
    $config = $this->configFactory->getEditable('config.instagram_field');
    $element['#cache'] = [
      'max-age' => $config->get('cachetime') * 60,
    ];
    $element['#attributes'] = [
      'class' => 'instagram-field',
    ];
    if ($config->get('accesstoken') === '') {
      $err_msg = $this->t("instagramautherror: No access token.");
      $this->logger->get('instagram_field')->warning($err_msg);
      return $element;
    }
    try {
      if ($config->get('accesstokenrefresh') > 0 && $config->get('accesstokenrefresh') < time()) {
        $request = $this->httpRequest('GET',
          'https://graph.instagram.com/refresh_access_token', [
            'query' => [
              'access_token' => $config->get('accesstoken'),
              'grant_type' => 'ig_refresh_token',
            ],
          ]
        );
        $result = Json::decode($request->getBody());
        if ($request->getStatusCode() === 200) {
          $config->set('accesstoken', preg_replace('/[^A-Za-z0-9.]/', '', $result['access_token']));
          $config->set('accesstokenrefresh', time() + (24 * 60 * 60));
        }
        else {
          $this->logger->get('instagram_field')->warning($request->getBody());
          return $element;
        }
      }
      $request = $this->httpRequest('GET',
        'https://graph.instagram.com/' . $config->get('userid') . '/media', [
          'query' => [
            'access_token' => $config->get('accesstoken'),
            'fields' => 'id,media_type,media_url,thumbnail_url,permalink',
            'count' => count($items),
          ],
        ]
      );
    }
    catch (RequestException $e) {
      $this->logger->get('instagram_field')->warning($e->getMessage());
      return $element;
    }
    $result = Json::decode($request->getBody());
    foreach ($result['data'] as $key => $value) {
      if (isset($items[$key])) {
        $this->loadImage($value['id'], ($value['media_type'] === 'VIDEO') ? ($value['thumbnail_url']) : ($value['media_url']));
        $items[$key]->setValue([
          'instagramid' => $value['id'],
          'instagramlink' => $value['permalink'],
          'instagramtype' => $value['media_type'],
        ], TRUE);
      }
    }

    foreach ($items as $delta => $item) {
      if (strlen($items[$delta]->getValue()['instagramid']) > 1) {
        $element[$delta] = [
          '#type' => 'html_tag',
          '#tag' => 'A',
          '#attributes' => [
            'href' => $items[$delta]->getValue()['instagramlink'],
            'class' => [
              'instagram-field-' . $items[$delta]->getValue()['instagramtype'],
            ],
          ],
          'content' => [
            'img' => [
              '#type' => 'html_tag',
              '#tag' => 'img',
              '#attributes' => [
                'src' => $this->fileUrlGenerator->generateString(self::THUMBS_DIRECTORY .
                '/' . $items[$delta]->getValue()['instagramid'] . '.jpg'),
              ],
            ],
          ],
        ];
      }
    }
    return $element;
  }

  /**
   * Load image from disk or download it from instagram.
   */
  private function loadImage($imageid, $thumbnail_url) {
    $directory = self::THUMBS_DIRECTORY;
    $local_uri = $directory . '/' . $imageid . '.jpg';
    if (!file_exists($local_uri)) {
      $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
      try {
        $thumbnail = $this->httpClient->request('GET', $thumbnail_url);
        $this->fileSystem->saveData((string) $thumbnail->getBody(),
          $local_uri);
      }
      catch (\Exception $e) {

      }
    }
  }

  /**
   * HttpClient request method enable test mock.
   */
  private function httpRequest($method, $uri, $options) {
    if (drupal_valid_test_ua()) {
      return new Response(200, [], file_get_contents(dirname(__FILE__) . '/../../../../tests/src/Functional/Mocks/instagram_users_self_media_recent.json'));
    }
    else {
      return $this->httpClient->request($method, $uri, $options);
    }
  }

}
