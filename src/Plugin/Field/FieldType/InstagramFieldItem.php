<?php

namespace Drupal\instagram_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'instagramfield' field type.
 *
 * @FieldType(
 *   id = "instagramfield",
 *   label = "Instagram Field",
 *   category = "Media",
 *   default_widget = "instagramfield_default",
 *   default_formatter = "instagramfield_formatter"
 * )
 */
class InstagramFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'instagramid' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'instagramlink' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'instagramtype' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['instagramid'] = DataDefinition::create('string')
      ->setLabel(t('Instagram user ID'));
    $properties['instagramlink'] = DataDefinition::create('string')
      ->setLabel(t('Instagram link'));
    // Instagram post type (Carousel, Video, Image).
    $properties['instagramtype'] = DataDefinition::create('string')
      ->setLabel(t('Instagram type'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('instagramid')->getValue();
    return $value === NULL || $value === '';
  }

}
