CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------
This module allows to use your recent Instagram post as a field in your
content type (or paragraph type), updates when cache timeout is reached.
Images and links are cached on your server.


REQUIREMENTS
------------

This module requires an Facebook developer account and an Instagram account


INSTALLATION
------------

Install the Instagram Field module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the instagram_field module
    2. Navigate to Administration > Configuration > Services > Instagram Field
    3. Register a new app at https://developers.facebook.com
       My Apps > Create App > Instagram: Set up >
       Products toolbar: Instagram Basic Display > Create New App
    4. Copy "Instagram App ID" and "Instagram App Secret" to Instagram Field
       settings and save
    5. Copy "OAuth redirect URI" from Instagram Field Settings to
       "Valid OAuth Redirect URIs"
    6. Add your Website URL to "Deauthorize Callback URL" and
       "Data Deletion Request URL"
    7. Roles -> Roles -> Instagram Testers -> Add Instagram Testers:
       your Instagram accountname
    8. Press "Authenticate" button to authenticate and get an access token


TROUBLESHOOTING
---------------

 * make sure that no webserver redirection is set for the callback URL (e.g.
   http -> https)

 * Note: Cache time out of all pages with Instagram fields depend on cache time
   of the Instagram field settings


MAINTAINERS
-----------

 * sleitner - https://www.drupal.org/u/sleitner
